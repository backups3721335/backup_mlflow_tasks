import os

from dotenv import find_dotenv, load_dotenv
from prefect import flow

import backup_common as backup
import backup_mlflow as mlflow_bp
from my_time import get_current_time


def read_env_variables():
    load_dotenv(find_dotenv())


@flow
def create_mlflow_backup(
    postgres_user,
    postgres_db,
    mlflow_docker_dir,
    postgres_container_name,
    minio_data,
    minio_backup_dir,
    minio_bucket_name,
    remote_server,
    server_folder,
):
    cur_time = get_current_time()
    cur_time = cur_time.strftime("%Y-%m-%d_%H-%M-%S_%Z")

    minio_s3_bucket_folder = os.path.join(minio_data, minio_bucket_name)

    mlflow_bp.stop_mlflow(mlflow_docker_dir)
    mlflow_bp.start_postgres_container(postgres_container_name)

    mlflow_bp.backup_mlflow_db(minio_backup_dir, cur_time, postgres_user, postgres_db)
    mlflow_bp.backup_minio_s3_data(minio_backup_dir, minio_s3_bucket_folder, cur_time)

    mlflow_bp.restart_mlflow(mlflow_docker_dir)

    backup.print_backup_files(minio_backup_dir)
    backup.upload_backup(minio_backup_dir, remote_server, server_folder)
    backup.backup_clean(minio_backup_dir)


if __name__ == "__main__":
    # считаем переменные в рабочее окружение
    read_env_variables()

    postgres_user = os.getenv("POSTGRES_USER")
    postgres_db = os.getenv("POSTGRES_DB")
    mlflow_docker_dir = os.getenv("MLFLOW_DOCKER_DIR")
    postgres_container_name = os.getenv("POSTGRES_CONTAINER_NAME")
    minio_data = os.getenv("MINIO_DATA")
    minio_backup_dir = os.getenv("MINIO_BACKUP_DIR")
    minio_bucket_name = os.getenv("MINIO_BUCKET_NAME")
    remote_server = os.getenv("REMOTE_SERVER")
    server_folder = os.getenv("SERVER_FOLDER")

    create_mlflow_backup(
        postgres_user,
        postgres_db,
        mlflow_docker_dir,
        postgres_container_name,
        minio_data,
        minio_backup_dir,
        minio_bucket_name,
        remote_server,
        server_folder,
    )
