import os
import backup_common as backup
import backup_mlflow as mlflow_bp
from dotenv import find_dotenv, load_dotenv
from prefect import flow


def read_env_variables():
    load_dotenv(find_dotenv())


@flow(log_prints=True)
def restore_mlflow_from_incremental_backup(
    postgres_user,
    postgres_db,
    mlflow_docker_dir,
    postgres_container_name,
    minio_data,
    minio_backup_dir,
    minio_bucket_name,
    remote_server,
    server_folder,
):
    minio_s3_bucket_folder = os.path.join(minio_data, minio_bucket_name)

    backup.backup_clean(minio_backup_dir)

    server_mlflow_dir = os.path.join(server_folder, os.path.split(minio_backup_dir)[1])
    backup.download_backup(
        server_mlflow_dir, remote_server, os.path.split(minio_backup_dir)[0]
    )
    backup.print_backup_files(minio_backup_dir)

    mlflow_bp.stop_mlflow(mlflow_docker_dir)
    mlflow_bp.start_postgres_container(postgres_container_name)

    mlflow_bp.restore_mlflow_db(minio_backup_dir, postgres_user, postgres_db)
    mlflow_bp.restore_minio_s3_folder(minio_backup_dir, minio_s3_bucket_folder)

    mlflow_bp.restart_mlflow(mlflow_docker_dir)

    backup.backup_clean(minio_backup_dir)


if __name__ == "__main__":
    # считаем переменные в рабочее окружение
    read_env_variables()

    postgres_user = os.getenv("POSTGRES_USER")
    postgres_db = os.getenv("POSTGRES_DB")
    mlflow_docker_dir = os.getenv("MLFLOW_DOCKER_DIR")
    postgres_container_name = os.getenv("POSTGRES_CONTAINER_NAME")
    minio_data = os.getenv("MINIO_DATA")
    minio_backup_dir = os.getenv("MINIO_BACKUP_DIR")
    minio_bucket_name = os.getenv("MINIO_BUCKET_NAME")
    remote_server = os.getenv("REMOTE_SERVER")
    server_folder = os.getenv("SERVER_FOLDER")

    restore_mlflow_from_incremental_backup(
        postgres_user,
        postgres_db,
        mlflow_docker_dir,
        postgres_container_name,
        minio_data,
        minio_backup_dir,
        minio_bucket_name,
        remote_server,
        server_folder,
    )
